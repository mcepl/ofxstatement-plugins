Collection of all known plugins to ofxstatement_.

Missing repo for ofxstatement-postfinance, which has URL at 
https://pypi.python.org/pypi/ofxstatement-postfinance, but its 
repository 
(https://github.com/maxandersen/ofxstatement-postfinance) is 
dead.

.. _ofxstatement:
    https://github.com/kedder/ofxstatement
